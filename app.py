from flask import Flask, render_template, abort, request
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database/db.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

# class TimestampedModel(db.Model):
#     __abstract__ = True
#
#     created_at = db.Column(db.DateTime, default=db.func.now())
#     updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

class Post(db.Model):
    __tablename__ = 'Posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    content = db.Column(db.Text)

    def __repr__(self):
        return '<Post : "{}">'.format(self.title)

# class NewPost():
#     p = Post()
#     p.title =
#     p.content = input('contenu : ')
#     db.session.add(p)
#     db.session.commit()

""" Fonction contextuel appelée dans footer.html """
@app.context_processor
def inject_now():
    return {'now': datetime.now()}

""" Fonction contextuel appelée dans index.html """
@app.context_processor
def utility_processor():
    def pluralize(count, singular, plural=None):
        if not isinstance(count, int):
            raise ValueError('"{}" must be an integer'.format(count))
        if plural is None:
            plural = singular + 's'
        if count == 1 or count == 0:
            string = singular
        else:
            string = plural

        return "{} {}".format(count, string)
    return dict(pluralize=pluralize)


"""			 Pages 				"""
@app.route('/')
def home():
    print('test')
    return render_template('pages/home.html')

@app.route('/blog')
def posts_index():
    posts = Post.query.all()
    return render_template('posts/index.html', posts=posts)

@app.route('/blog/post/<int:id>')
def posts_show(id):
    post = Post.query.get(id)

    if post is None:
        abort(404)
    return render_template('posts/show.html', post=post)

@app.route('/blog/newPost')
def newPost():
    return render_template('posts/newPost.html')


""" permet de créer un article dans la base de données """
@app.route('/blog/newPost', methods=['POST'])
def add_post():
    p = Post()
    p.title = request.form['title']
    p.content = request.form['article']
    db.session.add(p)
    db.session.commit()
    return render_template('pages/home.html')

""" tentative pour effacer un article de la base de données """
@app.route('/blog', methods=['POST'])
def delete_post():
    print('test')
    id = request.form['article']
    p = Post.query.get(int(id))
    db.session.delete(p)
    return render_template('pages/home.html')

""" gestion d'erreur 404 """
@app.errorhandler(404)
def page_not_found(error):
    return render_template('errors/404.html'), 404


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True, host='0.0.0.0', port=3000)
